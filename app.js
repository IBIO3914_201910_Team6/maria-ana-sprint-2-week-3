const reasonInput=document.querySelector('#input-reason')
const amountInput=document.querySelector('#input-amount')
const cancelBTN=document.querySelector('#btn-cancel')
const addBTN=document.querySelector('#btn-add')
const exList=document.querySelector('#ex-list')
const totalExOut=document.querySelector('#total-ex')
const totalSavings=document.querySelector('#total-savings')
const radioG=document.querySelector('#radio')

const alertCtrl = document.querySelector('ion-alert-controller');
let totalEx=0;
let totalSa=0;

const clear = () => {
    reasonInput.value='';
    amountInput.value='';
    
};

function formatDate(date) {
  var monthNames = [
    "January", "February", "March",
    "April", "May", "June", "July",
    "August", "September", "October",
    "November", "December"
  ];

  var day = date.getDate();
  var monthIndex = date.getMonth();
  var year = date.getFullYear();

  return monthNames[monthIndex]+ ' ' + day+'/' + year;
}

addBTN.addEventListener('click',()=>{
    
    const enteredReason = reasonInput.value;
    const enteredAmount = amountInput.value;
    var expDate =new Date();
    const type = radioG.value;
    
  // var day = expDate.getDate();
  // var monthIndex = expDate.getMonth();
  // var year = expDate.getFullYear();
    if (enteredReason.trim().length<=0||
    enteredAmount<=0||
    enteredAmount.trim().length<=0){

        alertCtrl
            .create({
                message:'Please enter valid reason and amount!',
                header:'Invalid Inputs',
                buttons:['Okay']})
            .then(alertElement=>{
                alertElement.present();
            });


        return;
    }
    


  const newItem = document.createElement('ion-item');
  
 
  console.log(type)
  if(type=='expense'){
    totalEx += +enteredAmount;
    totalExOut.textContent=totalEx;
    totalSa -= +enteredAmount;
    totalSavings.textContent=totalSa;
   
    newItem.style.color='warning'
    newItem.textContent = enteredReason +': $ ' + enteredAmount + '  on ' +formatDate(expDate);
  }
  else if(type =='income')
  {
    
    totalSa += +enteredAmount;
    totalSavings.textContent=totalSa;
    newItem.style.color='sucess'
    newItem.textContent = enteredReason +': $ ' + enteredAmount + '  on ' +formatDate(expDate);
  }
  

  exList.appendChild(newItem);

  clear();

});

cancelBTN.addEventListener('click',clear);
